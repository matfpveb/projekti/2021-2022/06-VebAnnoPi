const express = require('express');
const router = express.Router();

const multer = require('multer')
const path = require('path');
const fs = require('fs');
var parser = require('./parser.js');
const child_process = require('child_process').spawn;

const process = require('process');
process.chdir('routes/api/')

router.get('/files', (req, res, next) => {
    let dirFiles = fs.readdirSync(__uploadDir);
    response = [];
    dirFiles.forEach((file) => {
        let filepath = path.join(__uploadDir, file);
        let stat = fs.statSync(filepath);
        let line = { filename: file, size: stat.size, uploadTime: stat.birthtimeMs };
        response.push(line)
    })
    response.sort((a, b) => {
        return -(a.uploadTime - b.uploadTime);
    });
    res.send(response);
})

router.get('/streaming', (req, res) => {

    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Content-Type', 'text/event-stream');
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.flushHeaders(); // flush the headers to establish SSE with client

    let originUrl = req.headers.referer;


    interValID = setInterval(() => {

        if (terminalOutput[originUrl]) {
            console.log("sent to terminal", terminalOutput[originUrl]);
            res.write(`data: ${terminalOutput[originUrl]}\n`)
            terminalOutput[originUrl] = terminalOutput[originUrl].substring(terminalOutput[originUrl].indexOf('\n') + 1)
        }
    }, 500);

    // If client closes connection, stop sending events
    res.on('close', () => {
        clearInterval(interValID);
        res.end();
    });
});

const storage = multer.diskStorage({
    destination: __uploadDir,

    filename: (req, file, cb) => {
        let filename = file.originalname;
        cb(null, filename)
    }
})

const uploads = multer({ storage: storage })

router.post('/upload', uploads.single('vcffile'), (req, res, next) => {
    console.log("uploaded file " + req.file.originalname);
    res.status(201).end();
});

router.post('/script', (req, res) => {

    res.setHeader('Content-Type', 'routerlocation/json');
    console.log("Received request: " + req.body)
    let originUrl = req.headers.referer;
    terminalOutput[originUrl] = "";
    let file = req.body.filename;
    let filepath = path.join(__uploadDir, file);
    let outputFilename = file.substring(0, file.indexOf('.vcf'));

    // // Pokretanje python skripte i dohvatanje ispisa
    var annopi = child_process('python3', ['-u', 'AnnoPI.py', '-d', filepath, '-g', req.body.go, '-h', req.body.hpo]);
    annopi.stdout.on('data', (data) => {
        let message = String(data);
        terminalOutput[originUrl] += message;

    });
    annopi.stderr.on('error', (err) => {
        terminalOutput[originUrl] += String(err);
    })

    // // Kada skripta zavrsi sa radom, mozemo da uradimo cleanup - brisanje privremenih fajlova,... i da parsiramo html rezultat 
    annopi.on('close', (code) => {
        console.log('Annopi exited with code ' + code);

        if (code === 0) {
            let JSONToSend = parser.getJSON(outputFilename + '.html');
            res.json(JSONToSend);

            console.log("response sent");
        } else {
            terminalOutput[originUrl] += "Annopi error, exited with code: " + 0;
            console.log("Error")
        }
        if (fs.existsSync(outputFilename + '.html')) {
            fs.unlinkSync(outputFilename + '.html');
        }
        if (fs.existsSync(outputFilename + '.avinput')) {
            fs.unlinkSync(outputFilename + '.avinput');
        }
        if (fs.existsSync(outputFilename + '.hg19_multianno.txt')) {
            fs.unlinkSync(outputFilename + '.hg19_multianno.txt');
        }
        if (fs.existsSync(outputFilename + '.hg19_multianno.vcf')) {
            fs.unlinkSync(outputFilename + '.hg19_multianno.vcf');
        }
        if (fs.existsSync('GeneJSMap.js')) {
            fs.unlinkSync('GeneJSMap.js');
        }
        if (fs.existsSync('GOtooltipJS.js')) {
            fs.unlinkSync('GOtooltipJS.js');
        }
        if (fs.existsSync('HPOtooltipJS.js')) {
            fs.unlinkSync('HPOtooltipJS.js');
        }

    })
})

module.exports = router;