var HTMLParser = require('node-html-parser');
var fs = require('fs');
const res = require('express/lib/response');


function getJSONFromFile(path) {
  let JSONArray = []

  let html = fs.readFileSync(path);

  const root = HTMLParser.parse(html);
  let row = 0;
  let propertyNameCounter = 0;
  let propertyNames = ["Chr", "Begin", "End", "Ref", "Alt", "Exotic Function", "Gene", "GO Associations", "HPO Associations", "AllGO", "AllHPO"];

  root.querySelectorAll('tr')
    .forEach(trNode => {
      let resultJSON = {};
      resultJSON.id = row;
      propertyNameCounter = 0;

      trNode.querySelectorAll('td').forEach(
        tdNode => {

          currentProperty = propertyNames[propertyNameCounter++];
          if (currentProperty === "GO Associations" || currentProperty === "HPO Associations") {
            resultJSON[currentProperty] = tdNode.rawText.split(' ');
          } else if (currentProperty === "AllHPO" || currentProperty == "AllGO"){
            tdNode.childNodes.filter(child => child.rawTagName == 'a')
            .forEach(child => resultJSON[currentProperty] = String(child.rawAttrs).split(" ")[0].slice(6, -1));
          } else{
            resultJSON[currentProperty] = tdNode.rawText;
          }
          
        })
      JSONArray.push(resultJSON);
    });
  return JSONArray;
}
module.exports.getJSON = getJSONFromFile;