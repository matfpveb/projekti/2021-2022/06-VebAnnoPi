const path = require('path');
const fs = require('fs');



global.terminalOutput = {};
var IntervalID;

global.__uploadDir = path.join(__dirname, 'resources', 'uploads');
if (!fs.existsSync(__uploadDir)) {
    fs.mkdirSync(__uploadDir);
}


const express = require('express');
const mongoose = require('mongoose');
const { clearInterval } = require('timers');

app = express();
app.use(
    express.urlencoded({
        extended: false,
    })
);
app.use(express.json());

app.use('/', function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.setHeader('Connection', 'keep-alive');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');

        return res.status(200).json({});
    }
    req.setTimeout((0));
    next();
});

const annopiRouter = require('./routes/api/annopi')
app.use('/annopi', annopiRouter);

app.use(function (req, res, next) {
    const error = new Error('Zahtev nije podrzan!');
    error.status = 405;

    next(error);
});

app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
    res.status(statusCode).json({
        error: {
            message: error.message,
            status: statusCode,
            stack: error.stack,
        },
    });
});

module.exports = app;