const child_process = require('child_process').spawn;

var annopi = child_process('python3', ['-u', 'AnnoPI.py', '-d', './example/ex2.vcf']);

annopi.stdout.on('data' ,(data) => {
    console.log(String(data));
});

annopi.stderr.on('error', (err) =>{
    console.error(err);
})

annopi.on('close', (code) =>{
    console.log("exited with code ${code}");
})