import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  private indxDb: IDBFactory;

  constructor() {
    this.indxDb = window.indexedDB;

  }

  ConnectToDB(dbName: string): IDBOpenDBRequest {
    return this.indxDb.open(dbName, 8);
  }

  DestroyDB(dbName: string) {
    this.indxDb.deleteDatabase(dbName);
  }

  storeData(dbName: string, data: any) {
    let request = this.ConnectToDB(dbName);
    let db: IDBDatabase;
    request.onupgradeneeded = event => {
      // //Iz nekog razloga typescript ne prepoznaje da postoji event.target.result, pa mora preko any
      let tmp: any = event.target;
      db = tmp.result;
      let objectStore = db.createObjectStore("results")
      objectStore.put(data, "result")
    }
  }
}
