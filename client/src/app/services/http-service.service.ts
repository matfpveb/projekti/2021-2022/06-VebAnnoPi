import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {
  serverUrl = "http://localhost:3000/annopi"
  httpClient: HttpClient;

  constructor(private _zone: NgZone, httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public runAnnoPI(urlGO: string, urlHPO: string, fileName: string): Observable<HttpResponse<any>> {
    let totalUrl = this.serverUrl + "/script";
    let filename = fileName;
    let requestBody = { go: urlGO, hpo: urlHPO, filename: filename }
    return this.httpClient.post<any>(totalUrl, requestBody, { observe: "response" })
  }

  public getFiles(): Observable<HttpEvent<any>> {
    return this.httpClient.get<HttpEvent<any>>(this.serverUrl + "/files", { observe: "events" })
  }

  public uploadFile(file: File): Observable<HttpEvent<any>> {
    let formData = new FormData();
    formData.append("vcffile", file, file.name);
    return this.httpClient.post<HttpEvent<any>>(this.serverUrl + "/upload", formData, { reportProgress: true, observe: "events", });

  }

  public getTerminalStream() {
    return new Observable<string>(observer => {
      const eventSource = new EventSource(this.serverUrl + "/streaming")

      eventSource.onmessage = (message) => {
        this._zone.run(() => {
          observer.next(String(message.data))
        })
      }

      eventSource.onerror = error => {
        this._zone.run(() => {
          observer.error(error);
        })
      }
    })
  }
}
