import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-hpo-image-cell-renderer',
  templateUrl: './hpo-image-cell-renderer.component.html',
  styleUrls: ['./hpo-image-cell-renderer.component.css']
})
export class HpoImageCellRendererComponent implements ICellRendererAngularComp{

  params: any;
  geneValue: any;


  agInit(params: ICellRendererParams) {
    this.geneValue = params.data["AllHPO"];
  }

  refresh(): boolean {
    return false;
  }
}
