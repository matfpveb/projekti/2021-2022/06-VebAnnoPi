import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-go-cell-renderer',
  templateUrl: './go-cell-renderer.component.html',
  styleUrls: ['./go-cell-renderer.component.css']
})

export class GOCellRendererComponent implements ICellRendererAngularComp{

  params: any;
  cellValueGO: any;
  GOValues: any;


  agInit(params: ICellRendererParams) {
    this.GOValues = []
    this.params = params;
    this.cellValueGO = params.data["GO Associations"];
    if (this.cellValueGO) {
      for(let i = 0; i < this.cellValueGO.length; i++){
        if (this.cellValueGO[i].length > 0) {
          this.GOValues.push(this.cellValueGO[i])
        }
      }
    }
  }

  refresh(): boolean {
    return false;
  }
}
