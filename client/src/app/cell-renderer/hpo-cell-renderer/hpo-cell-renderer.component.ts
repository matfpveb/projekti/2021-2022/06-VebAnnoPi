import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-hpo-cell-renderer',
  templateUrl: './hpo-cell-renderer.component.html',
  styleUrls: ['./hpo-cell-renderer.component.css']
})
export class HpoCellRendererComponent implements ICellRendererAngularComp{

  params: any;
  cellValueHPO: any;
  HPOValues: any;


  agInit(params: ICellRendererParams) {
    this.HPOValues = [];
    this.params=params;
    this.cellValueHPO = params.data["HPO Associations"]
    if (this.cellValueHPO) {
      for(let i = 0; i < this.cellValueHPO.length; i++){
        if (this.cellValueHPO[i].length > 0) {
          this.HPOValues.push(this.cellValueHPO[i])
        }
      }
    }
  }

  refresh(): boolean {
    return false;
  }
}
