import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-go-image-cell-renderer',
  templateUrl: './go-image-cell-renderer.component.html',
  styleUrls: ['./go-image-cell-renderer.component.css']
})
export class GoImageCellRendererComponent implements ICellRendererAngularComp{

  params: any;
  geneValue: any;

  agInit(params: ICellRendererParams) {
    this.geneValue = params.data["AllGO"];
  }

  refresh(): boolean {
    return false;
  }
}
