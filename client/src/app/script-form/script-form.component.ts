import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpServiceService } from '../services/http-service.service';
import { DatePipe } from '@angular/common';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { Router } from '@angular/router';
import sizeof from 'object-sizeof';
import { DbService } from '../services/db.service';
import * as e from 'express';

interface FileInfo {
  filename: string,
  size: number,
  date: string | null,
}

@Component({
  selector: 'app-script-form',
  templateUrl: './script-form.component.html',
  styleUrls: ['./script-form.component.css']
})
export class ScriptFormComponent implements OnInit {

  urlHPO = "";
  urlGO = "";
  sendButtonDisabled = true;
  result: any;
  httpService: HttpServiceService;

  availableFiles: FileInfo[];
  selectedFile = "";

  clickedRow: number | null = null;

  terminalOutputStream: Observable<string> | null = null;
  terminalOutput: string = "";


  constructor(private router: Router, private DBService: DbService, httpService: HttpServiceService, formBuilder: FormBuilder) {
    this.httpService = httpService;
    this.availableFiles = [];

    this.httpService.getFiles().subscribe((result) => {
      if (result.type === HttpEventType.Response) {
        result.body.forEach((element: any) => {
          let pipe = new DatePipe('en-US');
          let dateString = pipe.transform(element.uploadTime, 'MMMM/d/yy, h:mm a');
          let fInfo: FileInfo = { date: dateString, filename: element.filename, size: element.size };
          this.availableFiles.push(fInfo);
        });
      }
    });
  }

  ngOnInit(): void {
  }

  resetTerminal() {
    this.terminalOutput = ""
  }

  updateTerminal(message: string) {
    this.terminalOutput += message + "\n";
  }


  initializeEventStream() {
    this.resetTerminal();
    this.terminalOutputStream = this.httpService.getTerminalStream();
    this.terminalOutputStream.subscribe(output => {
      this.updateTerminal(output)
    })
  }

  resetForm() {
    this.sendButtonDisabled = true;
    this.clickedRow = null;
    this.selectedFile = "";
  }

  runScript() {
    this.initializeEventStream();
    let file = this.selectedFile;
    this.resetForm();
    this.httpService.runAnnoPI(this.urlGO, this.urlHPO, file).subscribe(result => {
      this.DBService.DestroyDB("TestDB")
      this.DBService.storeData("TestDB", result.body);
      window.open("/results", "_blank")

    })
  }
  changeGO(e: any): void {
    this.urlGO = e.target.value;
  }

  changeHPO(e: any): void {
    this.urlHPO = e.target.value;
  }

  rowClicked(row: number) {
    this.selectedFile = this.availableFiles[row].filename;
    this.clickedRow = row;
    this.sendButtonDisabled = false;
  }

  rowClickedStyle(row: number): string {
    if (row == this.clickedRow) {
      return "bg-slate-600 text-zinc-200"
    } else {
      return "";
    }
  }

  sendButtonStyle(): string {
    if (this.sendButtonDisabled) {
      return "text-zinc-200 bg-blue-200"
    } else {
      return "bg-blue-400";
    }
  }
}
