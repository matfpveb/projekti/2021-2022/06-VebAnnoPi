import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {
  @Input() currentProgress: number = 0;
  constructor() { }

  ngOnInit(): void {
  }

  barColor(): string {
    if (this.currentProgress < 33) {
      return "bg-red-500"
    } else if (this.currentProgress < 66) {
      return "bg-yellow-500";
    } else {
      return "bg-green-500";
    }
  }
}
