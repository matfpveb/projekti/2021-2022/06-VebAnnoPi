import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileUploadFormComponent } from './file-upload-form/file-upload-form.component';
import { ResultTableComponent } from './result-table/result-table.component';
import { ScriptFormComponent } from './script-form/script-form.component';

const routes: Routes = [
  {path:'', component: ScriptFormComponent},
  {path:'results', component: ResultTableComponent},
  {path:'uploadform', component: FileUploadFormComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
