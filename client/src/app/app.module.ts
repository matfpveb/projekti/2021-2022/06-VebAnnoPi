import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ResultTableComponent } from './result-table/result-table.component';
import { ScriptFormComponent } from './script-form/script-form.component';
import { FileUploadFormComponent } from './file-upload-form/file-upload-form.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';

import { GOCellRendererComponent } from './cell-renderer/go-cell-renderer/go-cell-renderer.component';
import { HpoCellRendererComponent } from './cell-renderer/hpo-cell-renderer/hpo-cell-renderer.component';
import { GoImageCellRendererComponent } from './cell-renderer/go-image-cell-renderer/go-image-cell-renderer.component';
import { HpoImageCellRendererComponent } from './cell-renderer/hpo-image-cell-renderer/hpo-image-cell-renderer.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ResultTableComponent,
    ScriptFormComponent,
    FileUploadFormComponent,
    GOCellRendererComponent,
    HpoCellRendererComponent,
    GoImageCellRendererComponent,
    HpoImageCellRendererComponent,
    ProgressBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
