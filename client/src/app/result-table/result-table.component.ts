import { Component, OnInit } from '@angular/core';


import { HpoCellRendererComponent } from '../cell-renderer/hpo-cell-renderer/hpo-cell-renderer.component';
import { HpoImageCellRendererComponent } from '../cell-renderer/hpo-image-cell-renderer/hpo-image-cell-renderer.component';
import { GoImageCellRendererComponent } from '../cell-renderer/go-image-cell-renderer/go-image-cell-renderer.component';
import { GOCellRendererComponent } from '../cell-renderer/go-cell-renderer/go-cell-renderer.component';
import { ActivatedRoute } from '@angular/router';
import { HttpServiceService } from '../services/http-service.service';
import { DbService } from '../services/db.service';

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.css']
})
export class ResultTableComponent implements OnInit {


  errors = '';
  searchValue = '';
  rowData: any;
  colDefs: any;
  private gridApi: any;

  filename = "";

  constructor(private DBService: DbService, private route: ActivatedRoute, private httpService: HttpServiceService) {

  }

  ngOnInit(): void {

    let request = this.DBService.ConnectToDB("TestDB");
    request.onsuccess = event => {
      let tmp: any = event.target
      let db: IDBDatabase = tmp.result;
      let transaction = db.transaction("results", "readonly");
      let objectStore = transaction.objectStore("results")
      let objectStoreRequest = objectStore.get("result");
      objectStoreRequest.onsuccess = result => {
        this.rowData = objectStoreRequest.result
      }
      // this.DBService.DestroyDB("TestDB");
    }

  }


  onGridReady(params: any) {
    this.gridApi = params.api;
  }

  onBtnExportCSV() {
    //ime ce biti isto kao ime originalnog fajla. Na primer ex2.vcf prelazi u ex2.csv
    let params = {
      fileName: this.filename,
      onlySelected: true
    }
    //Bice exportovani svi selektovani redovi. Zbog toga se poziva funkcija koja ce da selektuje sve redove,
    //zatim se tabela exportuje i na kraju se deselektuju redovi kako ne bi cela tabela bila plava zato sto je selektovana
    this.gridApi.selectAllFiltered();
    this.gridApi.exportDataAsCsv(params);
    this.gridApi.deselectAllFiltered();
  }

  quickSearch() {
    this.gridApi.setQuickFilter(this.searchValue);
  }

  defaultColDef = {
    flex: 1,
    minWidth: 100,
    filter: true,
    sortable: true,
    resizable: true,
    wrapText: true,
    autoHeight: true,
    cacheQuickFilter: true,
  };

  columnDefs = [
    { field: 'Chr' },
    { field: 'Begin' },
    { field: 'End' },
    { field: 'Ref' },
    { field: 'Alt' },
    { field: 'Exotic Function', minWidth: 180 },
    { field: 'Gene' },
    {
      field: 'GO Associations',
      minWidth: 180,
      cellRendererFramework: GOCellRendererComponent,
    },
    {
      field: 'HPO Associations',
      headerName: 'HPO Annotations',
      minWidth: 180,
      cellRendererFramework: HpoCellRendererComponent,
    },
    {
      field: 'AllGO',
      headerName: 'All GO',
      cellRendererFramework: GoImageCellRendererComponent,
    },
    {
      field: 'AllHPO',
      headerName: 'All HPO',
      cellRendererFramework: HpoImageCellRendererComponent,
    },
  ];

}
