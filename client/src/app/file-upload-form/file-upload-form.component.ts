import { HttpEventType } from '@angular/common/http';
import { Component, ElementRef, OnInit, Output, ViewChild } from '@angular/core';
import { HttpServiceService } from '../services/http-service.service';
import { ProgressBarComponent } from '../progress-bar/progress-bar.component';

@Component({
  selector: 'app-file-upload-form',
  templateUrl: './file-upload-form.component.html',
  styleUrls: ['./file-upload-form.component.css']
})
export class FileUploadFormComponent implements OnInit {

  selectedFile: File | null = null;

  uploadStatus = "";
  @Output() uploadProgress: number = 0;
  uploadButtonDisabled = true;

  showProgressBar = false;
  @ViewChild("file") filePickerButton: any;


  constructor(private httpSevice: HttpServiceService) { }

  ngOnInit(): void {
  }


  uploadFile() {
    this.uploadStatus = "Uploading";
    this.showProgressBar = true;
    if (this.selectedFile)
      this.httpSevice.uploadFile(this.selectedFile).subscribe(result => {
        if (result.type == HttpEventType.UploadProgress) {
          if (result.total) {
            this.uploadProgress = (result.loaded * 100.0) / result.total;
          }
        } else if (result.type == HttpEventType.Response) {
          this.uploadStatus = "File Uploaded"
          this.showProgressBar = false;
          this.uploadButtonDisabled = true;
          this.selectedFile = null;
        }
      });
  }

  pickFile() {
    this.filePickerButton.nativeElement.click();
  }

  displaySelectedFile(): string {
    if (this.selectedFile == null) {
      return "No File Selected";
    } else {
      return this.selectedFile.name;
    }
  }
  changeSelectedFile(e: any) {
    this.uploadStatus = ""
    this.selectedFile = e.target.files[0];
    this.uploadButtonDisabled = false;
  }

  uploadButtonStyle(): string {
    if (this.uploadButtonDisabled) {
      return "bg-blue-200 text-zinc-200 cursor-default"
    } else {
      return "bg-blue-400 hover:bg-blue-800 hover:text-zinc-200 cursor-pointer"
    }
  }


}
