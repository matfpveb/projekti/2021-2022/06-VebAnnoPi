## CSV Export

CSV Export sada exportuje samo filtrirane redove  

Dodata funkcionalnost za ime fajla. Umesto export.csv, fajl ce sada imati isto ime kao originalni fajl na osnovu kog je zapoceta skripta. Na primer, ex2.vcf ce se eksportovati ex2.csv

## Rutiranje

Cela aplikacija je razbijena na 3 komponente, script-form, result-table i file-upload-form od kojih svaka ima svoju rutu. Ovo omogucava citljiviji kod i jednostavniji GUI.

## File upload i pokretanje skripte

Prethodno je potencijalno bio veliki problem sto su fajlovi za upload preveliki da bi se svaki put slali preko interneta.
Ovo je reseno tako sto je slanje fajlova i pokretanje skripte odvojeno. Sada se fajlovi salju na ruti "/uploadform"  

Kada se otvori aplikacija, salje se GET zahtev serveru, koji ce da odgovoriti sa listom svih dostupnih fajlova koji su prethodno poslati.
Nakon toga ce ti fajlovi biti prikazani i mozemo da izaberemo koji cemo da koristimo.

## Tabela sa rezultatima

Tabela je ima svoju rutu "/results" i otvara se u novom tabu.
Kada rezultati stignu, stavljaju se u lokalnu bazu podataka pomocu IndexedDB sistema. 
Zatim se otvara novi tab, gde se ti rezultati citaju a zatim se citava baza brise.

## Misc

Do sada je aplikacija funkcionisala tako sto je imala nekoliko promenljivih i funkcija koje prate stanje i prikazuju odgovarujucu komponentu pomocu ngIf direktiva.
Sada je sve ovo izbaceno zbog rutiranja i komponente su mnogo manje pa se lakse razumeju.

Resen problem DOM manipulacije.
