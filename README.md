# Project VebAnnoPi

Bioinformatička aplikacija za praćenje nukleotidnih varijacija

## Developers

- [Radomir Vujosevic, 380/2020](https://gitlab.com/RadomirVujosevic)
- [Sanja Radulović, 15/2016](https://gitlab.com/Sanja-Radulovic)

## Demo

AnnoPI veb demo:
https://www.youtube.com/watch?v=f20tZ-_mwv0&feature=youtu.be



## Uputstva za pokretanje

Pre pokretanja neophodno je skinuti annovar program:
https://www.openbioinformatics.org/annovar/annovar_download_form.php
Registracija je neophodna ali besplatna. Nakon registracije stici ce vam email sa download linkom.

Nakon toga, potrebno je sve fajlove i foldere koji se nalazi u Annovar folderu staviti u server folder

Program se pokrece pomocu naredbi:

Server:
cd server
node server.js

Client:
cd client
ng serve

Sa klijentske strane potrebno je prvo izabrati jedan .vcf fajl a zatim pritisnuti dugme Send.
U annovar folderu se nalazi folder example koji sadrzi .vcf fajlove. 
Ovi primeri su mali pa je savet da se koristi neki od njih kao sto je ex2.vcf
Ocekivano vreme rada je nekoliko minuta, od zavisnosti od vaseg racunara.

Ukoliko zelite da koristite vece ulazne fajlove kao test primere, mozete ih naci u AnnoPI projektu, u AnnovarInput direktorijumu
https://drive.google.com/drive/u/0/folders/1hWBu-rfSVpLgaAUm-aU5jZvqcij2Z0qJ

Na pocetku submit forme se nalaze 2 text input polja. Ona sluze za unos linkova koji su opcioni argumenti -g i -h u AnnoPI skripti.
Mogu se ostaviti prazna


